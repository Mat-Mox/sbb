/// @description Insert description here
// You can write your code in this editor
//show_debug_message(yPosStart);

//show_debug_message(string(spaceHoldTime))



if(gameStartRequest) // pressed space once from menu
{
	if (counter = true){
		audio_play_sound(_321goooo, 10, 0)
		audio_stop_sound(SBB_THEME)
		audio_play_sound(SBB_THEME, 10, 0)
		counter = false	
	}
	sprite_index = poses[0]
	oPlayer.image_angle = 0;
	if (countdown > 0)
	{
		gameStartTimer = string(3 - (int64((current_time - timeBegin)/1000)));
		countdown = 3 - (int64((current_time - timeBegin)/1000))	
	}
	else
	{
		gameStartTimer = "Go !";
		alarm[0] = 100 // removes go later
		
		gameStarted = true;
		gameStartRequest = false;
			
		timeBegin = current_time;
	}

}

if(gameStarted)
{
	
	if (current_time - timeBegin > hitDuration && canHit == true) // quand le timer est à zéro
	{
		isFreezed = true;
		canHit = false;
	
	
	}
	else // quand c'est pas fini et qu'il se fait fracasser
	{
		timeRemaining = (hitDuration - (current_time - timeBegin)) / 1000;
		if (timeRemaining <= 0)
			timeRemaining = 0;
		
		
	}
	
	if (isFreezing)
	{
		counter = true
		
		audio_play_sound(critical_hit, 10, 0)
		var t = current_time + 1000;
		while (current_time < t) { 
			}
		isFreezing = false
		audio_play_sound(blowaway_l, 10, 0)
	}


	if (isFreezed){//durant le freeze
		capTime = count * 200; // + random
		hittime = current_time;
		V = Vmax;
	
		// delay to freeze
		isFlying = true;
		isFreezed = false;
		
		oMonster.sprite_index = FINAAAL_FLAAAASH
		isFreezing = true
		land = true
	
	}
	
	

	if (isFlying){//durant le vol
		oPlayer.image_angle -= V/1.5; // roll
		
		sprite_index = fly_2

		if (current_time - hittime < capTime) 
		{
			pScore += Vmax * count * 10 / 25;
		}
	
		else
	
		{
			if (V > 0)
			{
				V -= descentSpeed * 1 / count;
				pScore += V* count * 10 / 25;	
			}
			else
			{
				V = 0;
				isFlying = false;
			}
		}
	
		// Moving the sprite up and down
		oPlayer.y = lerp(oPlayer.y, -V * 20 + yPosStart, 0.1);
	
	}

	
	if(!canHit && !isFreezed && !isFlying) // between two games
	{
		
		oPlayer.image_angle = 0;
		sprite_index = crash_2
		
		if (land == true)
		{
			audio_play_sound(landing, 10, 0)
			oCamController.shake=true
			oCamController.alarm[0]=10
			land = false
		}
		
		
		gameStarted=false
		gameStartRequest=false
	}
}
else
{
	// Moving the sprite up and down
	oPlayer.y = lerp(oPlayer.y, yPosStart, 0.1);
}