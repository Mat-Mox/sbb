// @description Insert description here
// You can write your code in this editor

//angle = oPlayer.V
angle = lerp(angle, (oPlayer.V) + 150, .01)

part_type_direction(redParticle, angle - redAngle, angle + redAngle, 0, 4)
part_type_direction(laserParticle, angle - laserAngle, angle + laserAngle, 0, 4)
part_type_direction(starParticle, 180 - starAngle, 180 + starAngle, 0, 4)
part_type_direction(circleParticle, angle - circleAngle, angle + circleAngle, 0, 4)
part_type_direction(sparkParticle, angle - sparkAngle, angle + sparkAngle, 0, 4)
	
	
	
	
part_emitter_region(sparkParticleSystem, sparkEmitter, oPlayer.x, oPlayer.x, oPlayer.y, oPlayer.y, ps_shape_ellipse, ps_distr_linear)
part_emitter_region(circleParticleSystem, circleEmitter, oPlayer.x, oPlayer.x, oPlayer.y, oPlayer.y, ps_shape_ellipse, ps_distr_linear)
part_emitter_region(laserParticleSystem, laserEmitter, oPlayer.x-200, oPlayer.x, oPlayer.y, oPlayer.y, ps_shape_ellipse, ps_distr_linear)
part_emitter_region(redParticleSystem, redEmitter, oPlayer.x-200, oPlayer.x, oPlayer.y, oPlayer.y, ps_shape_ellipse, ps_distr_linear)
part_emitter_region(starParticleSystem, starEmitter, 1920, 1920, 0, 1080, ps_shape_rectangle, ps_distr_linear)

if (!oPlayer.isFlying && on)
{
	part_emitter_clear(sparkParticleSystem, sparkEmitter);
	part_emitter_clear(circleParticleSystem, sparkEmitter);
	part_emitter_clear(laserParticleSystem, laserEmitter);
	part_emitter_clear(starParticleSystem, starEmitter);
	part_emitter_clear(redParticleSystem, redEmitter);
	
	
	
	
	
    on = false
}
else if (oPlayer.isFlying && !on)
{
	
	angle = 210
	
	part_type_direction(redParticle, 210 - redAngle, 210 + redAngle, 0, 4)
	part_type_direction(laserParticle, 210 - laserAngle, 210 + laserAngle, 0, 4)
	part_type_direction(starParticle, 180 - starAngle, 180 + starAngle, 0, 4)
	part_type_direction(circleParticle, 210 - circleAngle, 210 + circleAngle, 0, 4)
	part_type_direction(sparkParticle, 210 - sparkAngle, 210 + sparkAngle, 0, 4)

	// BLUE SPARK //
	sparkParticleSystem = part_system_create()
    part_system_depth(sparkParticleSystem, 100)
    sparkEmitter = part_emitter_create(sparkParticleSystem)
	
    part_emitter_stream(sparkParticleSystem, sparkEmitter, sparkParticle, -sparkCount)
	
	// WHITE CIRCLE //
	circleParticleSystem = part_system_create()
    part_system_depth(circleParticleSystem, 100)
    circleEmitter = part_emitter_create(circleParticleSystem)
	
    part_emitter_stream(circleParticleSystem, circleEmitter, circleParticle, -circleCount)
	
	// LASER //
	laserParticleSystem = part_system_create()
    part_system_depth(laserParticleSystem, 100)
    laserEmitter = part_emitter_create(laserParticleSystem)
	
    part_emitter_stream(laserParticleSystem, laserEmitter, laserParticle, -laserCount)
	
	// star //
	starParticleSystem = part_system_create()
    part_system_depth(starParticleSystem, 100)
    starEmitter = part_emitter_create(starParticleSystem)
    part_emitter_stream(starParticleSystem, starEmitter, starParticle, -starCount)
	
	// red //
	redParticleSystem = part_system_create()
    part_system_depth(redParticleSystem, 100)
    redEmitter = part_emitter_create(redParticleSystem)
    part_emitter_stream(redParticleSystem, redEmitter, redParticle, -redCount)
	 
    on = true
}