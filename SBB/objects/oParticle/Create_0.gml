/// @description Insert description here
// You can write your code in this editor

angle = 180

// BLUE SPARK //
sparkAngle = 20
sparkCount = 50
sparkParticleSystem = part_system_create()
part_system_depth(sparkParticleSystem, 100)
sparkParticle = part_type_create()
part_type_shape(sparkParticle, pt_shape_flare)
part_type_orientation(sparkParticle, 0, 0, 0, 0, 1)
part_type_size(sparkParticle, 0.20, .25, 0, 0)
part_type_scale(sparkParticle, 5, .5)
part_type_speed(sparkParticle, 50, 100, 0.05, 0)
part_type_life(sparkParticle, 100, 250)
sparkEmitter = part_emitter_create(sparkParticleSystem)


// WHITE CIRCLE //
circleAngle = 5
circleCount = 15
circleParticleSystem = part_system_create()
part_system_depth(circleParticleSystem, 100)
circleParticle = part_type_create()
part_type_sprite(circleParticle, Illustration_sans_titre, 1, 0, 0)
//part_type_shape(circleParticle, pt_shape_ring)
part_type_orientation(circleParticle, 0, 0, 0, 0, 1)
part_type_size(circleParticle, .2, .2, 0.01, 0)
part_type_scale(circleParticle, 1, 1)
part_type_speed(circleParticle, 50, 100, 0.05, 0)
part_type_life(circleParticle, 100, 250)
circleEmitter = part_emitter_create(circleParticleSystem)

// LASER //
laserAngle = 0
laserCount = 2
laserParticleSystem = part_system_create()
part_system_depth(laserParticleSystem, 100)
laserParticle = part_type_create()
part_type_shape(laserParticle, pt_shape_smoke)
part_type_orientation(laserParticle, 0, 0, 0, 0, 1)
part_type_size(laserParticle, 1, 1.1, 0, 0)
part_type_scale(laserParticle, 4, .5)
part_type_speed(laserParticle, 50, 100, 0.05, 0)
part_type_life(laserParticle, 100, 250)
laserEmitter = part_emitter_create(laserParticleSystem)

// STARS //
starAngle = 0
starCount = 2
starParticleSystem = part_system_create()
part_system_depth(starParticleSystem, 100)
starParticle = part_type_create()
part_type_shape(starParticle, pt_shape_line)
part_type_orientation(starParticle, 0, 0, 0, 0, 1)
part_type_size(starParticle, .5, 1, 0, 0)
part_type_scale(starParticle, 17, .5)
part_type_speed(starParticle, 800, 900, 0.05, 0)
part_type_life(starParticle, 100, 250)
starEmitter = part_emitter_create(starParticleSystem)

// RED //
redAngle = 0
redCount = 5
redParticleSystem = part_system_create()
part_system_depth(redParticleSystem, 100)
redParticle = part_type_create()
part_type_sprite(redParticle, Sprite38, 1, 0, 0)
//part_type_shape(redParticle, Sprite38)
part_type_orientation(redParticle, 0, 0, 0, 0, 1)
part_type_size(redParticle, .6, .8, 0, 0)
part_type_scale(redParticle, 1, 1)
part_type_speed(redParticle, 0, 0, 0.05, 0)
part_type_life(redParticle, 5, 7)
redEmitter = part_emitter_create(redParticleSystem)


on = false

