/// @description Insert description here
// You can write your code in this editor
if (oPlayer.isFlying){
	self.x -= oPlayer.V*2;
	self.y = lerp (self.y, 1080+oPlayer.V*65, .1)
}
else 
{
	self.y = lerp (self.y, 1080, .1)
}

//apparaitre le bg suivant & s'autodétruit
if (self.x <= -1920)
{
	isCloud = random_range(1,2)
	if(isCloud < 1.2)
	{
		randCloud = random_range(1,5)
		if(randCloud < 2.5)
		{
			instance_create_layer(self.x+1920*3, 1080, "Objects", oCoulds1)
		}
		else if (randCloud >= 2.5 && randCloud < 4)
		{
			instance_create_layer(self.x+1920*3, 1080, "Objects", oCoulds2)
		}
		else if (randCloud >= 4)
		{
			instance_create_layer(self.x+1920*3, 1080, "ObjectsSolid", oCoulds3)
		}
	}
	
	
	isObj = random_range(1,2)
	if(isObj < 1.2)
	{
		randplanet = random_range(1,4)
		while (oObjController.lastPlanet == int64(randplanet)) 
		{
			randplanet = random_range(1,4)
		}
	
		if(randplanet < 2)
		{
			instance_create_layer(self.x+1920*3, 1080, "ObjectsSolid", oPlanet1)
		}
		else if (randplanet >= 2 && randplanet < 3)
		{
			instance_create_layer(self.x+1920*3, 1080, "ObjectsSolid", oPlanet2)
		}
		
		else if (randplanet >= 3)
		{
			instance_create_layer(self.x+1920*3, 1080, "ObjectsSolid", oDarkStar)
		}
		oObjController.lastPlanet = int64(randplanet)
	}
		
	isObj = random_range(1,2)
	if(isObj < 1.2)
	{
		randobj = random_range(1,3)
		while (oObjController.lastObj == int64(randobj)) 
		{
			randobj = random_range(1,3)
		}
		
		if (randobj < 2)
		{
			instance_create_layer(self.x+1920*3, 1080, "ObjectsSolid", oComet)
		}
		else if(randobj >= 2)
		{
			instance_create_layer(self.x+1920*3, 1080, "ObjectsSolid", oOvni)
		}
		oObjController.lastObj = int64(randobj)
	}
	
	instance_create_layer(self.x+1920*3, 1080, "BG4", oBG4)
	instance_destroy(self)
}
