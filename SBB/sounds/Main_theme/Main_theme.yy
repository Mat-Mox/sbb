{
  "name": "Main_theme",
  "compression": 0,
  "type": 0,
  "sampleRate": 44100,
  "bitDepth": 1,
  "bitRate": 128,
  "volume": 0.5,
  "preload": false,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "Main_theme.wav",
  "duration": 172.805344,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "tags": [],
  "resourceType": "GMSound",
}