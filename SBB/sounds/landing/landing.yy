{
  "name": "landing",
  "compression": 0,
  "type": 0,
  "sampleRate": 44100,
  "bitDepth": 1,
  "bitRate": 128,
  "volume": 1.0,
  "preload": false,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "landing.wav",
  "duration": 1.835458,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "tags": [],
  "resourceType": "GMSound",
}