{
  "name": "SBB_THEME",
  "compression": 0,
  "type": 0,
  "sampleRate": 44100,
  "bitDepth": 1,
  "bitRate": 128,
  "volume": 0.4,
  "preload": false,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "SBB_THEME.mp3",
  "duration": 61.1054077,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "tags": [],
  "resourceType": "GMSound",
}